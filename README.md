## My Favorite Artist.

This application uses a form input to take in your favorite artist, which is pre-populated with one of my favorite artists and will find and display the albums of the specified artist via the iTunes API.

See yarn start script below for running locally.

## Available Scripts

In the project directory, you can run:

### `yarn install`

Install dependencies, required prior to running yarn start.

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

