import React from 'react';
import AlbumsDisplay from './AlbumsDisplay';
import axios from 'axios';

export default class Artist extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      artist: 'Pete Rock & C.L. Smooth',
      albums: [],
      error: null
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) { this.setState({artist: event.target.value}); }

  getAlbumsByArtist(artist){
    axios
      .get("search?term=" + encodeURIComponent(artist) + "&media=music&entity=album&attribute=artistTerm")
      .then(response => {
        this.setState({
          albums : response.data.results
        });
      })
      .catch(error => this.setState({ error, isLoading: false }));
  }

  handleSubmit(event) {
    event.preventDefault();
    this.getAlbumsByArtist(this.state.artist);
  }

  componentDidMount() {
    this.getAlbumsByArtist(this.state.artist);
  }

  render() {
    return (
      <>
        <form onSubmit={this.handleSubmit} className="mb-5" >        
          <label>
            Enter Artist: 
            <input type="text" value={this.state.artist} onChange={this.handleChange} />
          </label>
          <input type="submit" value="Submit" />
        </form>
        <AlbumsDisplay albums={this.state.albums} error={this.state.error}/>
      </>
    );
  }
}