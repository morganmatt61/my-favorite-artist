import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import AlbumCard from './AlbumCard';

export default class AlbumsDisplay extends React.Component {
  render() {
    const albums = this.props.albums
    if ( this.props.error ) {
      let error =this.props.error
      return (
        <h5>Error: {error.message}</h5>
      )  
    }
    if ( albums === undefined || albums.length === 0 ) {
      return (
        <h5>No Results Found</h5>
      );
    }
    return (
      <Container>
        <Row>
          {
            albums.map((album, index) => (
              <Col lg={6} xl={4} className="mb-5" key={index}>
                <AlbumCard album={album} />
              </Col>
            ))
          }
        </Row>
      </Container>
    );
  }
}