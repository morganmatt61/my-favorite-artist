import React from 'react';
import { Row, Col } from 'react-bootstrap';

export default class AlbumsCard extends React.Component {
  _getYearFromDate(date) {
    date = new Date(date);
    return date.getFullYear();
  }

  render() {
    const album = this.props.album;
    return (
      <div className="Album-card mr-2 h-100">
        <Row className="h-100">
          <Col xs={4} className="my-auto">
            <a href={album.collectionViewUrl}  target="_blank" rel="noopener noreferrer">
              <img src={album.artworkUrl100}  alt={album.collectionName}/>
            </a>
          </Col>
          <Col xs={8}>
            <div className="d-flex flex-column justify-content-around h-100">
              <p><strong>{album.collectionName}</strong></p>
              <p>{album.artistName}, {this._getYearFromDate(album.releaseDate)}</p>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}