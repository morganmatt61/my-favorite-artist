import './App.css';
import Artist from './components/Artist';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
      <header className="App-header py-3 mb-5">
        <h1>Favorite Artist Albums</h1>
      </header>
      <Artist />
    </div>
  );
}

export default App;
